﻿using System;
using System.Collections.Generic;

namespace ASTest
{
    public static class ArrayHelper
    {
        public static int CountDistinctElements<T>(T[] array)
        {
            int result = 0;
            List<T> distinctItems = new List<T>();
            for(int i = 0; i < array.Length; i++)
            {
                if(i == 0)
                {
                    distinctItems.Add(array[1]);
                    result++;
                }
                bool isDistinct = false;

                for(int j = 0; j < distinctItems.Count; j++)
                {
                    if(distinctItems[j].Equals(array[i]))
                    {
                        break;
                    }

                    isDistinct = true;
                }
                if(isDistinct)
                {
                    distinctItems.Add(array[i]);
                    result++;
                }
            }

            return result;
        }
    }
}