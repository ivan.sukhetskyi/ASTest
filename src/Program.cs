﻿using System;
using System.Diagnostics;

namespace ASTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 2, 53543, 2};
            Console.WriteLine($"The count of distinct elements is: {ArrayHelper.CountDistinctElements(array)}");
        }
    }
}
