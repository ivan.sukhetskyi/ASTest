namespace ASTest
{
    public static class Math
    {
        public static long Square(long n)
        {
            if (n == 0)
                return 0;

            if (n < 0)
                n = -n;

            var x = n >> 1;
            if(n % 2 != 0)
                return ((Square(x) << 2) + (x << 2) + 1);
            else
                return (Square(x) << 2);
        }
    }
}